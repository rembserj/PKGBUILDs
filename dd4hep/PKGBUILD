# Maintainer:  gagnonlg NOSPAM protonmail NOSPAM com
# See also: https://github.com/paulgessinger/dd4hep-aur
# edited by Jonas Rembser to compile without problem and adapt to newest dd4hep version

pkgname=dd4hep
_pkgver=01-25-01
pkgver=$(echo $_pkgver | sed 's/-/./g')
pkgrel=1
pkgdesc="Detector Description Toolkit for High Energy Physics"
arch=('x86_64')
url="https://dd4hep.web.cern.ch/dd4hep/"
license=('LGPL3')
depends=('xerces-c'
	 'geant4>=10.2.2'
	 'root>=6.08'
	 'boost-libs>=1.49'
	 'intel-tbb'
	 'cern-vdt'
        )
makedepends=('boost>=1.49' 'cmake>=3.12')
source=(https://github.com/AIDASoft/$pkgname/archive/v$_pkgver.tar.gz)
md5sums=('7338b76495a8d707369f9dffb1a246f5')

prepare () {
  _pyver="$(python -V | awk '{print $2}')"
  sed -i "s/SET(REQUIRE_PYTHON_VERSION \${ROOT_PYTHON_VERSION})/SET(REQUIRE_PYTHON_VERSION \"$_pyver\")/" \
      "$srcdir/DD4hep-$_pkgver"/cmake/DD4hepBuild.cmake
  sed -i "s/INTERFACE_INCLUDE_DIRECTORIES \"\${Geant4_INCLUDE_DIRS}\"/INTERFACE_INCLUDE_DIRECTORIES \"\/usr\/\${Geant4_INCLUDE_DIRS}\"/" \
      "$srcdir/DD4hep-$_pkgver"/cmake/DD4hepBuild.cmake
}

build() {
  cd "$srcdir/DD4hep-$_pkgver"
  mkdir -p build && cd build

  # NOTE: build currently fails with latest tbb, so turned off for now

  cmake -DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_CXX_STANDARD=17 \
	-DDD4HEP_USE_GEANT4=ON \
	-DDD4HEP_USE_TBB=OFF \
	-DDD4HEP_USE_XERCESC=ON \
	..

  make -j$((`nproc`+1)) || return 1
}

package() {
  cd "$srcdir/DD4hep-$_pkgver/build"

  make DESTDIR="${pkgdir}" install
}
