import os

suspicious_packages = []

for root, dirs, files in os.walk(".", topdown=False):

    if not "BuildFile.xml" in files or not root.count("/") == 2:
        continue

    if "src" in dirs:
        continue

    suspicious_packages.append(root[2:])

for package in suspicious_packages:

    print(package)

    pkg = package.replace("/", "\/")
    os.system(f'git --no-pager grep "{package}" | grep BuildFile')
    oldstring = f'<use name=\\"{pkg}\\"\/>'
    newstring = f'<use name=\\"{pkg}\\" source_only=\\"1\\"\/>'
    cmd = f"grep -rl '{oldstring}' . | xargs sed -i 's/{oldstring}/{newstring}/g'"
    print(cmd)
    os.system(cmd)

    print("")
