cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_STANDARD 17)
project(CMSSW)

set(CUDAToolkit_ROOT "/opt/cuda")
set(CMAKE_CUDA_COMPILER "/opt/cuda/bin/nvcc")
set(CMAKE_CUDA_ARCHITECTURES "75")
enable_language(CUDA)

# temporary fix until I figure out how to properly treat transient include
# directories
include_directories("/opt/cuda/include")

# TODO: Some packages don't make their eigen dependency clear. This is not a
# great situation. Maybe it needs fixing upstream in cmssw...
#include_directories("/usr/include/tensorflow")
# See other comment further down to understand why eigen from TensorFlow

# Dependency management

find_library(clhep_LIBRARIES "CLHEP" "/usr/lib")
find_library(fmt_LIBRARIES "fmt" "/usr/lib")
find_library(gperf_LIBRARIES "profiler" "/usr/lib")
find_library(hepmc_LIBRARIES "HepMC" "/usr/lib")
find_library(heppdt_LIBRARIES "HepPDT" "/usr/lib")
find_library(libuuid_LIBRARIES "uuid" "/usr/lib")
find_library(md5_LIBRARIES "cms-md5" "/usr/lib")
find_library(openssl_LIBRARIES "crypto" "/usr/lib")
find_library(python3_LIBRARIES "python3.11" "/usr/lib")
find_library(tbb_LIBRARIES "tbb" "/usr/lib")
find_library(tensorflow-cc_LIBRARIES "tensorflow_cc" "/usr/lib")
find_library(tinyxml2_LIBRARIES "tinyxml2" "/usr/lib")
find_library(xerces-c_LIBRARIES "xerces-c" "/usr/lib")
find_library(xz_LIBRARIES "lzma" "/usr/lib")
find_library(zlib_LIBRARIES "z" "/usr/lib")

find_package(Eigen3 3.3 REQUIRED NO_MODULE)

find_package(Boost COMPONENTS filesystem program_options thread regex serialization REQUIRED)

list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS Core MathCore GenVector TMVA)
include(${ROOT_USE_FILE})

set(CoralBase_LIBRARIES CoralBase)
set(CoralCommon_LIBRARIES CoralCommon RelationalAccess)
set(CoralKernel_LIBRARIES CoralKernel)
set(boost_LIBRARIES ${Boost_LIBRARIES})
set(boost_serialization_LIBRARIES ${Boost_LIBRARIES})
set(curl_LIBRARIES curl)
set(eigen_LIBRARIES "Eigen3::Eigen")
set(roofit_LIBRARIES ${ROOT_LIBRARIES})
set(root_LIBRARIES ${ROOT_LIBRARIES})
set(rootcling_LIBRARIES ${ROOT_LIBRARIES})
set(rootcore_LIBRARIES ${ROOT_LIBRARIES})
set(roothistmatrix_LIBRARIES ${ROOT_LIBRARIES})
set(rootmath_LIBRARIES ${ROOT_LIBRARIES})
set(roottmva_LIBRARIES ${ROOT_LIBRARIES})

set(XrdCl_INCLUDE_DIRECTORIES "/usr/include/xrootd" "/usr/include/xrootd/private")
set(boost_python_INCLUDE_DIRECTORIES "/usr/include/python3.11")
set(catch2_INCLUDE_DIRECTORIES "/usr/include/catch2")
set(cuda_INCLUDE_DIRECTORIES "/opt/cuda/include")
set(rocm_INCLUDE_DIRECTORIES "/opt/rocm/hip/include")
# We need to use eigen from TensorFlow, otherwise TensorFlow headers will not compile.
set(eigen_INCLUDE_DIRECTORIES "/usr/include/tensorflow")
set(geant4_INCLUDE_DIRECTORIES "/usr/include/Geant4")
set(geant4core_INCLUDE_DIRECTORIES "/usr/include/Geant4")
set(python3_INCLUDE_DIRECTORIES "/usr/include/python3.11")
set(sigcpp_INCLUDE_DIRECTORIES "/usr/include/sigc++-2.0")
set(tensorflow-cc_INCLUDE_DIRECTORIES "/usr/include/tensorflow")
set(tensorflow_INCLUDE_DIRECTORIES "/usr/include/tensorflow")
set(xrootd_INCLUDE_DIRECTORIES "/usr/include/xrootd" "/usr/include/xrootd/private")


# Macros for dictionary generation

macro(GET_INCLUDE_COMPILER_LIST MY_TARGET)

    get_target_property(MY_INTERFACE_LINK_LIBRARIES ${MY_TARGET} INTERFACE_LINK_LIBRARIES)
    list(APPEND MY_INTERFACE_LINK_LIBRARIES ${MY_TARGET})

    set(INCLUDE_COMPILER_LIST "-I${PROJECT_SOURCE_DIR}/cmssw")

    # Construct the compiler string for the include directories.
    foreach(lib ${MY_INTERFACE_LINK_LIBRARIES})
        if (TARGET ${lib})
            get_target_property(MY_INCLUDE_DIRECTORIES ${lib} INCLUDE_DIRECTORIES)
            foreach(dir ${MY_INCLUDE_DIRECTORIES})
                if(NOT ${dir} MATCHES "${PROJECT_SOURCE_DIR}")
                    list(APPEND INCLUDE_COMPILER_LIST "-I${dir}")
                endif()
            endforeach()
        endif()
    endforeach()

endmacro()


macro(MAKE_GENREFLEX_COMMAND MY_TARGET MY_SOURCE MY_HEADER MY_XML)

    add_custom_command(OUTPUT ${MY_SOURCE}.cxx
        COMMAND genreflex ${CMAKE_CURRENT_SOURCE_DIR}/src/${MY_HEADER}.h
        -s ${CMAKE_CURRENT_SOURCE_DIR}/src/${MY_XML}.xml
        -o ${MY_SOURCE}.cxx
        --rootmap=${CMAKE_BINARY_DIR}/lib/lib${MY_TARGET}_${MY_SOURCE}.rootmap
        --rootmap-lib=${CMAKE_BINARY_DIR}/lib/lib${MY_TARGET}.so
        --library ${CMAKE_BINARY_DIR}/lib/lib${MY_TARGET}.so --multiDict
        -DCMS_DICT_IMPL -D_REENTRANT -DGNUSOURCE -D__STRICT_ANSI__ -DCMSSW_REFLEX_DICT ${INCLUDE_COMPILER_LIST}
        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/src/${MY_HEADER}.h ${CMAKE_CURRENT_SOURCE_DIR}/src/${MY_XML}.xml
    )

endmacro()

# compiler flags

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O2 -Wno-deprecated-declarations -Wno-attributes -Wno-unused-result -msse3 -lrt -DTBB_PREVIEW_RESUMABLE_TASKS=1 -DTBB_SUPPRESS_DEPRECATED_MESSAGES -DTBB_PREVIEW_TASK_GROUP_EXTENSIONS=1")

set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-relaxed-constexpr")

# go into the cmssw source directory
add_subdirectory(cmssw)

# since we don't want to build with Python2 support, exclude the
# relevant build targets
set_target_properties(CondDBV2PyInterface PROPERTIES EXCLUDE_FROM_ALL 1)
