import glob
import os
import xml.etree.ElementTree as ET
from collections import defaultdict


class CMakeLists(object):
    def __init__(self):
        self._txt = []
        self._target_started = False

    def add_subdirs(self, directories):
        def add_with_guard(directory):
            self._txt += [f"if(CMSSW_BUILD_{directory.upper()})", f"    add_subdirectory({directory})", "endif()"]

        for d in directories:
            if d in ["bin"]:
                continue
            if d in ["test", "plugins"]:
                add_with_guard(d)
            else:
                self._txt += [f"add_subdirectory({d})"]

        return self

    def __iadd__(self, rhs):
        self._txt += rhs
        return self

    def add_interface_library(self):
        if self._target_started:
            raise RuntimeError("CMakeLists object already has an added target, fill it's name first!")

        # In CMake, one should optimally specify the interface files after the INTERFACE property.
        # But since this is CMSSW, this does not make much sense here.
        # http://mariobadr.com/creating-a-header-only-library-with-cmake.html
        # https://cmake.org/cmake/help/latest/command/add_library.html#interface-libraries
        self._txt += [
            "add_library(<target>)",
        ]
        self._target_started = True

    def add_library(self, source_files):
        if self._target_started:
            raise RuntimeError("CMakeLists object already has an added target, fill it's name first!")

        self._txt += [
            "add_library(<target> SHARED " + " ".join(source_files) + ")",
            "set_target_properties(<target> PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)",
            "install(TARGETS <target> LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR} OPTIONAL)",
        ]
        self._target_started = True

    def add_executable(self, source_files):
        if self._target_started:
            raise RuntimeError("CMakeLists object already has an added target, fill it's name first!")

        self._txt += [
            "add_executable(<target> " + " ".join(source_files) + ")",
            "set_target_properties(<target> PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)",
            "install(TARGETS <target> DESTINATION ${CMAKE_INSTALL_BINDIR} OPTIONAL)",
        ]
        self._target_started = True

    def fill_target(self, target):
        self._txt = list(map(lambda l: l.replace("<target>", target), self._txt))
        self._target_started = False

    def write(self, path):
        self._path = os.path.join(os.getcwd(), path)

        text = self._txt
        if type(text) == list:
            text = "\n".join(text)

        if "<target>" in text:
            raise RuntimeError("There are still <target> placeholders in the CMake file " + path)

        filename = os.path.join(self._path, "CMakeLists.txt")
        if not os.path.exists(filename):
            with open(filename, "w") as f:
                f.write(text)
        with open(filename, "r") as f:
            old_text = f.read()
        if old_text != text:
            with open(filename, "w") as f:
                f.write(text)


def name(elem):
    if not elem.get("name") is None:
        return elem.get("name")
    return elem.get("Name")


def load_config(config_file):
    import yaml

    with open(config_file, "r") as f:
        config = yaml.safe_load(f)

    for subsystem, subsystem_config in config["subsystems"].items():
        if subsystem_config is not None:
            if "whitelist" in subsystem_config and "blacklist" in subsystem_config:
                raise ValueError("Package config can't contain blacklist and whitelist at the same time.")
        else:
            config["subsystems"][subsystem] = {}

    return config


def cmake_dependency_lines(dependency, config):
    # deal with local cmssw deps
    if "/" in dependency:
        return ["", "target_link_libraries(<target> " + dependency.replace("/", "") + ")"]

    cmake = ["target_include_directories(<target> PUBLIC ${" + dependency + "_INCLUDE_DIRECTORIES})"]
    cmake += ["target_link_libraries(<target> ${" + dependency + "_LIBRARIES})"]

    return cmake


def complete_build_file(build_file, default_lib_name=None, in_plugin_dir=False):
    with open(build_file) as f:
        xml = f.read()

    # Remove the "iftool"s, which we don't support yet
    filtered_lines = []
    iftool_counter = 0
    for line in xml.split("\n"):
        if "<iftool" in line and not "cuda" in line:
            iftool_counter += 1
        elif "<iftool" in line:
            iftool_counter = 0
        elif "</iftool" in line:
            iftool_counter = 0
        elif iftool_counter == 0:
            filtered_lines.append(line)

    xml = "\n".join(filtered_lines)

    # skip empty build files
    if xml == "":
        return None

    auto = not "<bin" in xml and not "<library" in xml

    if auto and in_plugin_dir:
        xml = '<flags EDM_PLUGIN="1"/>\n' + xml
        default_lib_name = default_lib_name + "Plugins"
    if auto:
        xml = '<library   file="*.cc" name="' + default_lib_name + '">\n' + xml + "\n</library>"

    return xml


def parse_elements(root_node, config):
    cmake = []
    flags = {"LCG_DICT_HEADER": [], "LCG_DICT_XML": []}

    for elem in root_node:
        if elem.tag == "flags":
            if elem.get("cppflags"):
                cmake += ['set_target_properties(<target> PROPERTIES COMPILE_FLAGS "' + elem.get("cppflags") + '")']
                cmake += ['set_target_properties(<target> PROPERTIES PREFIX "plugin")']
            if elem.get("EDM_PLUGIN"):
                cmake += ['set_target_properties(<target> PROPERTIES PREFIX "plugin")']
            if elem.get("LCG_DICT_HEADER"):
                flags["LCG_DICT_HEADER"] = elem.get("LCG_DICT_HEADER").split(" ")
            if elem.get("LCG_DICT_XML"):
                flags["LCG_DICT_XML"] = elem.get("LCG_DICT_XML").split(" ")
        if elem.tag == "use" or elem.tag == "lib" and name(elem) != "1":
            if elem.get("source_only") == "1":
                continue
            cmake += cmake_dependency_lines(name(elem), config)
        if elem.tag == "lib":
            cmake += ["target_link_libraries(<target> " + name(elem) + ")"]

    return cmake, flags


def interpret_files(root, file_expressions):
    # sometimes there is space separation instead of comma
    file_expressions = file_expressions.replace(" ", ",")

    files = []
    for f in file_expressions.split(","):
        if "*" in f:
            globbed = glob.glob(os.path.join(root, f))
            files += [g[len(root) + 1 :] for g in globbed]
        else:
            files.append(f)
    return files


def make_lcg_source_names(n_dicts):
    sources = []
    if n_dicts == 1:
        sources.append("<target>_xr.cxx")
    if n_dicts > 1:
        for i in range(1, n_dicts + 1):
            sources.append(f"<target>_x{i}r.cxx")
    return sources


def lcg_dict_sources(headers, xmls):
    sources = make_lcg_source_names(len(headers))

    cmake = []
    if len(sources) > 0:
        cmake += ['list(APPEND SOURCE_FILES "' + '" "'.join(sources) + '")']

    return cmake


def lcg_dict_generation(headers, xmls):
    cmake = ["", "get_include_compiler_list(<target>)", ""]

    # TODO: remove ugly hack
    if len (headers)> 0:
        cmake +=["include_directories(\"/usr/include/eigen3\")", ""]


    for header, xml, source in zip(headers, xmls, make_lcg_source_names(len(headers))):
        cmake += ["make_genreflex_command(<target> " + source[:-4] + " " + header[:-2] + " " + xml[:-4] + ")"]

    return cmake


def get_global_dependencies(root_node):
    global_dependencies = []

    for elem in root_node:
        if elem.tag == "use" and elem.get("source_only") != "1":
            global_dependencies.append(name(elem))
        if elem.tag == "environment":
            for sub_elem in elem:
                if sub_elem.tag == "use":
                    global_dependencies.append(name(sub_elem))

    return global_dependencies


def wrie_cmake_for_target(cmake, root, elem, n_src_files, global_dependencies, config):
    target = name(elem)

    files = interpret_files(root, elem.get("file"))

    if target is None:
        target = files[0].split(".")[0]

    cmake_from_build_file, flags = parse_elements(elem, config)

    if not len(flags["LCG_DICT_HEADER"]) == 0 in flags and os.path.exists(os.path.join(root, "src/classes.h")):
        flags["LCG_DICT_HEADER"] = ["classes.h"]
        flags["LCG_DICT_XML"] = ["classes_def.xml"]

    cmake += lcg_dict_sources(flags["LCG_DICT_HEADER"], flags["LCG_DICT_XML"])

    is_interface = False

    if elem.tag == "library":
        n_files = n_src_files + len(files) + len(flags["LCG_DICT_HEADER"])
        if n_files == 0:
            is_interface = True
            cmake.add_interface_library()
        else:
            cmake.add_library(["${SOURCE_FILES}"] + files)
    if elem.tag == "bin":
        cmake.add_executable(files)

    for dependency in global_dependencies:
        cmake += cmake_dependency_lines(dependency, config)

    cmake += cmake_from_build_file

    cmake.fill_target(target + " INTERFACE" * (0 + is_interface))

    cmake += lcg_dict_generation(flags["LCG_DICT_HEADER"], flags["LCG_DICT_XML"])

    cmake.fill_target(target)


def build_xml_to_cmake(root, root_node, config):
    cmake = CMakeLists()

    n_globbed_files = len(glob.glob(os.path.join(root, "src/*.cc")))

    cmake += ['file(GLOB_RECURSE SOURCE_FILES "src/*.cc")']

    global_dependencies = get_global_dependencies(root_node)

    for elem in root_node:
        if elem.tag in ["library", "bin"]:
            wrie_cmake_for_target(cmake, root, elem, n_globbed_files, global_dependencies, config)

    return cmake


def cmssw_to_cmake(cmssw_src_dir, config_path):
    config = load_config(config_path)

    def compile_package(subsystem, pkg):
        if not subsystem in config["subsystems"]:
            return False
        cfg = config["subsystems"][subsystem]

        return not (
            "whitelist" in cfg and not pkg in cfg["whitelist"] or "blacklist" in cfg and pkg in cfg["blacklist"]
        )

    os.chdir(cmssw_src_dir)

    cmake_in_subsystem = defaultdict(dict)

    for root, directories, files in os.walk("."):
        if root.count("/") < 2:
            continue

        subsystem, package = root.split("/")[1:3]

        if not compile_package(subsystem, package):
            continue

        def accept_as_build_file(filename):
            return filename == "BuildFile.xml" and root.count("/") <= 3 and not root.endswith("python")

        for file in filter(accept_as_build_file, files):
            xml = complete_build_file(
                os.path.join(root, file),
                default_lib_name=subsystem + package,
                in_plugin_dir=os.path.basename(root) == "plugins",
            )
            if xml is None:
                continue
            # don't write cmake files immediately, we might want to change them later
            cmake_in_subsystem[subsystem]["/".join(root.split("/")[2:])] = build_xml_to_cmake(
                root, ET.fromstring("<root>" + xml + "</root>"), config
            )

    # Finally, include all subdirs into which we have written CMakeList files
    cmake = CMakeLists()
    cmake += ["include_directories(.)", ""]
    for subsystem, cmakes in cmake_in_subsystem.items():
        cmake.add_subdirs([subsystem])

        packages = sorted(list(set(map(lambda p: p.split("/")[0], cmakes.keys()))))

        subdirs_to_include = {package: [] for package in packages}

        for dir_in_subsystem in cmakes.keys():
            package = dir_in_subsystem.split("/")[0]
            if "/" in dir_in_subsystem:
                subdirs_to_include[package].append("/".join(dir_in_subsystem.split("/")[1:]))

        subdirs_included = {package: len(subdirs_to_include[package]) == 0 for package in packages}

        for dir_in_subsystem, cmake_i in cmakes.items():
            package = dir_in_subsystem.split("/")[0]
            if not "/" in dir_in_subsystem:
                cmake_i.add_subdirs(subdirs_to_include[package])
                subdirs_included[package] = True
            cmake_i.write(os.path.join(subsystem, dir_in_subsystem))

        for package in subdirs_to_include:
            if not subdirs_included[package]:
                CMakeLists().add_subdirs(subdirs_to_include[package]).write(os.path.join(subsystem, package))

        CMakeLists().add_subdirs(packages).write(subsystem)

    cmake.write(".")

    os.chdir("..")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Generate CMakeLists for CMSSW.")
    parser.add_argument("config", type=str, help="path to yaml config file")

    args = parser.parse_args()

    cmssw_to_cmake("cmssw", args.config)
